﻿import random
from time import sleep
from timeit import default_timer as timer
from typing import Tuple, List

from winsound import Beep

from woopy import woopy

ivec2 = Tuple[int, int]


class SnakeGame:
    # 🐍 🐍 🐍 🐍 🐍 🐍 🐍 🐍 🐍 🐍 🐍 🐍 🐍 🐍 🐍 🐍 🐍 🐍 🐍 🐍 🐍 🐍 🐍 🐍 🐍 🐍 🐍
    # 🐍                       SNAKE GAME FOR WOOTING                        🐍
    # 🐍                                                                     🐍
    # 🐍    Control the snake with arrow keys to get food.                   🐍
    # 🐍    The further you press the keys, the faster the snake will move   🐍
    # 🐍    and the more points you will get.                                🐍
    # 🐍                                                                     🐍
    # 🐍    Have fun playing.                                                🐍
    # 🐍 🐍 🐍 🐍 🐍 🐍 🐍 🐍 🐍 🐍 🐍 🐍 🐍 🐍 🐍 🐍 🐍 🐍 🐍 🐍 🐍 🐍 🐍 🐍 🐍 🐍 🐍

    _extends = 6, 14  # type: ivec2
    _start = 3, 2  # type: ivec2
    _blacklist = [(0, 1), (5, 3), (5, 4), (5, 5), (5, 6),
                  (5, 7), (5, 8), (5, 9), (4, 12)]  # type: List[ivec2]
    _arrow_keys = [(4, 15), (5, 15), (5, 14), (5, 16)]  # type: List[ivec2]

    def __init__(self, keyboard: woopy.Wooting, base_speed: int = 1, max_speed: int = 15,
                 layout: str = 'iso', skip_invisible_keys: bool = True) -> None:
        """
        Start a snake game on Wooting.

        :param keyboard: Wooting object to communicate with the keyboard.
        :param base_speed: Base speed of the snake if no key is pressed.
        :param max_speed: Maximum speed of the snake if key is fully pressed.
        :param layout: Keyboard layout to blacklist the correct keys, either 'iso' or 'ansi'.
        :param skip_invisible_keys: Flag if keys that are physically on your keyboard should be skipped.

        Note:
        Speed can be compared to fps, a speed of 1 updates every second, a speed of 2 twice a second, ...
        Above a speed of 25 the snake is displayed in simpler colors to allow fast enough updates.
        Maximum that can be displayed is around 200.
        """

        # GENERAL
        self.kbd = keyboard  # type: woopy.Wooting
        self.running = True  # type: bool
        self.speed_current = base_speed  # type: int
        self.speed_base = base_speed  # type: int
        self.speed_max = max(base_speed, max_speed)  # type: int
        self.skip_invisible_keys = skip_invisible_keys  # type: bool
        if layout == 'iso':
            self._blacklist += [(2, 13)]
        elif layout == 'ansi':
            self._blacklist += [(4, 1), (3, 12)]

        # STATS
        self.points = 0  # type: int
        self.highscore_points = 0  # type: int
        self.highscore_size = 0  # type: int
        self.rounds = 0  # type: int
        self.speed_history = []  # type: List[float]

        # SNAKE
        self.direction = 0, 0  # type: ivec2
        self.snake = [SnakeGame._start] # type: List[ivec2]
        self.old_tail = 0, 0

        # FOOD
        self.food = 0, 0  # type: ivec2
        self.food_time = 0.0
        self._food_spawn()

        for column in range(17):
            for row in range(6):
                if (row, column) != (SnakeGame._start and self.food):
                    self.kbd.set_key(row, column, 0, 0, 0)
                    sleep(0.01)

    def run(self) -> None:
        """ Run main game loop until stopped with ESC. """

        while self.running:
            self._snake_move()
            self._food_check()
            if self.speed_max > 25:
                self._draw_speed()
            else:
                self._draw()

            start = timer()
            while timer() - start < 1 / self.speed_current:
                self._control()
                if bool(self.kbd.read(0, 0)):
                    self.stop()
                sleep(0.01)

    def reset(self, win: bool = False) -> None:
        """ Reset game state. """

        self._update_highscore()
        self.points = 0
        self.rounds += 1
        self.direction = 0, 0
        self.snake = [SnakeGame._start]
        self.speed_history.clear()

        if win:
            self._draw_wipe(0, 255, 0)
        else:
            self._draw_wipe(255, 0, 0)
        self._food_spawn()

    def stop(self) -> None:
        """ Close the game and display summary message. """

        self._update_highscore()
        self.rounds += 1
        print(f'\nYou played {self.rounds} game{"s" if self.rounds > 1 else ""}.\n'
              f'Your highscore is {self.highscore_points} points by eating {self.highscore_size} keys.'
              f'\nThanks for playing.')
        self.speed_current = -1
        self.running = False
        self.kbd.reset()

    def _draw(self) -> None:
        """ Display snake and food on the keyboard, up to ~25 fps. """

        self.kbd.set_array_full([0] * 378)
        color = 255  # type: int
        dim = int(255 / max(len(self.snake) - 1, 1))
        for x, y in self.snake:
            self.kbd.set_array_single(x, y, color, 0, 255 - color)
            color -= dim
        self.kbd.set_array_single(*self.food, 0, 255, 0)
        self.kbd.update()

    def _draw_speed(self) -> None:
        """ Display a simpler snake optimized for speed, up to ~200 fps. """

        self.kbd.set_key(*self.snake[0], 0, 0, 255)
        if len(self.snake) > 1:
            self.kbd.set_key(*self.snake[1], 255, 0, 0)
        self.kbd.set_key(*self.old_tail, 0, 0, 0)

    def _draw_wipe(self, red: int, blue: int, green: int) -> None:
        """ Play a wipe animation. """

        for row in range(6):
            for column in range(17):
                self.kbd.set_key(row, column, red, blue, green)
                sleep(0.01)

        for column in range(17):
            for row in range(6):
                if (row, column) != SnakeGame._start:
                    self.kbd.set_key(row, column, 0, 0, 0)
                    sleep(0.01)

    def _snake_move(self) -> None:
        """ Update snake position based on current direction. """

        if self.direction == (0, 0):
            return
        old_head = self.snake[0]
        while True:
            new_head = ((old_head[0] + self.direction[0]) % self._extends[0],
                        (old_head[1] + self.direction[1]) % self._extends[1])  # type: ivec2
            # check for game over
            if new_head in self.snake:
                print(f'sssnek ded :(\nYou scored {self.points} points '
                      f'by eating {len(self.snake) - 1} keys.')
                self.reset()
                return
            if self.skip_invisible_keys and new_head in self._blacklist:
                old_head = new_head
                continue
            break

        # update snake body
        self.snake.insert(0, new_head)
        self.old_tail = self.snake.pop()
        self.speed_history.append(self.speed_current)

    def _food_check(self) -> None:
        """ Check if snake eats food, if true grow snake, add points and spawn new food. """

        if self.food == self.snake[0]:
            Beep(1500, 50)
            self.snake.append(self.snake[-1])
            # point multiplier for picking up food in under 10 sec
            food_speed_bonus = max(10 - (timer() - self.food_time), 1)
            # point multiplier for high average snake speed
            snake_speed_bonus = 1 / (sum([1 / x for x in self.speed_history]) / len(self.speed_history))
            bonus = int(len(self.snake) * (snake_speed_bonus + food_speed_bonus))
            self.points += bonus
            print(f'{self.points} (+{bonus})')
            self.speed_history.clear()
            self._food_spawn()

    def _food_spawn(self):
        """ Spawn new food at a valid position. """

        if len(set(self.snake + self._blacklist)) >= self._extends[0] * self._extends[1]:
            print('Found no place to spawn new food, I guess you completed the game.')
            self.points += 1337
            [Beep(int(2 ** (8 + int([x, -2][x == '8']) / 10)), 2 ** 9) for x in
             '445775420024422844577542002420082240245408245420228445775420024200']
            self.reset(True)

        while True:
            new_position = (random.randint(0, self._extends[0] - 1),
                            random.randint(0, self._extends[1] - 1))
            if new_position not in (self.snake + self._blacklist):
                self.food = new_position
                self.food_time = timer()
                break
        self.kbd.set_key(*self.food, 0, 255, 0)

    def _control(self) -> None:
        """ Check arrow keys and update snake direction and speed. """

        # controls = [up, down, left, right]
        controls = [self.kbd.read(*x) for x in self._arrow_keys]
        self.speed_current = (self.speed_max - self.speed_base) / 255 * max(controls) + self.speed_base
        controls = [1 if x else 0 for x in controls]
        row = controls[1] - controls[0]
        column = controls[3] - controls[2]
        if row or column:
            self.direction = row, column

    def _update_highscore(self):
        """ Update highscore. """

        if self.points > self.highscore_points:
            self.highscore_points = self.points
            self.highscore_size = len(self.snake) - 1
            print('New Highscore!')


if __name__ == '__main__':
    print('SNAKE FOR WOOTING\n\n'
          'Control the snake with arrow keys to get food.\n'
          'The further you press the keys, the faster the snake will move\n'
          'and the more points you will get.\n\n'
          'Have fun playing.\n')

    print('Choose the following settings, continue or skip with pressing ENTER.')
    base_speed = input('Please enter the base speed of the snake (default is 1): ') or '1'
    if not base_speed.isdigit():
        print('No valid number given, using default speed instead.')
        base_speed = '1'

    max_speed = input('Please enter the maximum speed of the snake (default is 15): ') or '15'
    if not max_speed.isdigit():
        print('No valid number given, using default speed instead.')
        max_speed = '15'

    layout = input('Please type your keyboard layout (iso, ansi): ') or 'iso'

    kbd = woopy.Wooting('libs/wooting-analog-sdk.dll', 'libs/wooting-rgb-sdk.dll')
    if kbd.connected():
        print('Wooting connected, ready to play.\nPress ESC to stop the game.')
        SnakeGame(kbd, int(base_speed), int(max_speed), layout).run()
    else:
        print('No Wooting connected :(')

    input('\nPress ENTER to close.')
