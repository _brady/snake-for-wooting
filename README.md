# Snake for Wooting
> Simple snake game to play on your Wooting keyboard.

![Snake for Wooting - Logo](/uploads/42f5e05914a6645ba7d3ee82d7ca95b7/snake.png)

Snake is played with and on your keyboard, the window is only used to
display some instructions and your achieved points.
The game uses the Wooting RGB SDK to display the snake on your keyboard
and the Analog SDK to increase the speed the further you press down the arrow keys.

For those that just want to play, you can download a compiled application [here](/uploads/d6be0d2cff2f526602f36a95660558d2/Snake.exe) with no further dependencies, otherwise refer to [Installation](#installation).

## Installation

Currently only works on Windows with 64bit Python 3:

To get started clone this project, download or compile the Wooting SDK DLLs and put them in /libs folder.

```sh
git clone --recursive https://gitlab.com/_brady/snake-for-wooting.git
```
For the Wooting SDK DLLs refer to:
* Analog SDK: https://github.com/WootingKb/wooting-analog-sdk
* RGB SDK: https://github.com/WootingKb/wooting-rgb-sdk

## Usage example

Just run ``snake.py`` and have fun.

## Release History

* 1.0
    * Release

## Meta

[Brady](https://gitlab.com/_brady/) – Contact me on the Wooting [Discord server](https://discordapp.com/invite/rNghtgw)

Distributed under the MIT license. See [LICENSE](LICENSE) for more information.